# 9ball

Riced randomizer for all of your non-life decisions!

[![Project Status: Moved to https://codeberg.org/MKware/MKRS – The project has been moved to a new location, and the version at that location should be considered authoritative.](https://www.repostatus.org/badges/latest/moved.svg)](https://www.repostatus.org/#moved) to [https://codeberg.org/MKware/MKRS](https://codeberg.org/MKware/MKRS)

***

![screenshot](https://i.imgur.com/BCMLLr4.png)

9ball is logical continuation of 8ball, with revamped UI, new shaking animations etc.

